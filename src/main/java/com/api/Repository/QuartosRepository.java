package com.api.Repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.api.Entity.Quartos;

@Repository
public interface QuartosRepository extends JpaRepository<Quartos, Long> {

}