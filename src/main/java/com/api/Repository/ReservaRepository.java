package com.api.Repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.api.Entity.Reserva;

public interface ReservaRepository extends JpaRepository<Reserva, Long>{

}
