package com.api.Repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.api.Entity.Comanda;

public interface ComandaRepository extends JpaRepository<Comanda, Long> {

}