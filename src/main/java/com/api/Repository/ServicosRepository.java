package com.api.Repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.api.Entity.Servicos;

public interface ServicosRepository extends JpaRepository<Servicos, Long>{

}