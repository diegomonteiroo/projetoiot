package com.api.Repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.api.Entity.Estabelecimento;

public abstract interface EstabelecimentoRepository extends JpaRepository<Estabelecimento, Long> {

}