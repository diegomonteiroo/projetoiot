package com.api.Repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.api.Entity.Cliente;

public interface ClienteRepository extends JpaRepository<Cliente, Long> {

}