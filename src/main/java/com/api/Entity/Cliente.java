package com.api.Entity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Entity
public class Cliente implements Serializable {
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long cod_Cliente;
	
	@NotBlank(message = "O campo é obrigatório")
	private String nome_Cliente;
	
	@NotBlank(message = "O campo é obrigatório")
	private String sobreNome_Cliente;
	
	@NotBlank(message = "O campo é obrigatório")
	private String email_Cliente;
	
	@NotNull(message = "Campo não pode ser vazio!")
	private int telefone_Cliente;
	
	@NotBlank(message = "O campo é obrigatório")
	private String endereco_Cliente;
	
	@NotNull(message = "Campo não pode ser vazio!")
	private int cep_Cliente;
	
	@NotNull(message = "Campo não pode ser vazio!")
	private int num_Endereco;
	
	@NotBlank(message = "O campo é obrigatório")
	private String compl_End_Cliente;
	
	@OneToMany(mappedBy = "cliente", cascade = CascadeType.ALL, fetch = FetchType.LAZY, targetEntity = Reserva.class)
	private List<Reserva> reserva;
	
	public Long getCod_Cliente() {
		return cod_Cliente;
	}

	public void setCod_Cliente(Long cod_Cliente) {
		this.cod_Cliente = cod_Cliente;
	}

	public String getNome_Cliente() {
		return nome_Cliente;
	}

	public void setNome_Cliente(String nome_Cliente) {
		this.nome_Cliente = nome_Cliente;
	}

	public String getSobreNome_Cliente() {
		return sobreNome_Cliente;
	}

	public void setSobreNome_Cliente(String sobreNome_Cliente) {
		this.sobreNome_Cliente = sobreNome_Cliente;
	}

	public String getEmail_Cliente() {
		return email_Cliente;
	}

	public void setEmail_Cliente(String email_Cliente) {
		this.email_Cliente = email_Cliente;
	}

	public int getTelefone_Cliente() {
		return telefone_Cliente;
	}

	public void setTelefone_Cliente(int telefone_Cliente) {
		this.telefone_Cliente = telefone_Cliente;
	}

	public String getEndereco_Cliente() {
		return endereco_Cliente;
	}

	public void setEndereco_Cliente(String endereco_Cliente) {
		this.endereco_Cliente = endereco_Cliente;
	}

	public int getCep_Cliente() {
		return cep_Cliente;
	}

	public void setCep_Cliente(int cep_Cliente) {
		this.cep_Cliente = cep_Cliente;
	}

	public int getNum_Endereco() {
		return num_Endereco;
	}

	public void setNum_Endereco(int num_Endereco) {
		this.num_Endereco = num_Endereco;
	}

	public String getCompl_End_Cliente() {
		return compl_End_Cliente;
	}

	public void setCompl_End_Cliente(String compl_End_Cliente) {
		this.compl_End_Cliente = compl_End_Cliente;
	}

	public List<Reserva> getReserva() {
		return reserva;
	}

	public void setReserva(List<Reserva> reserva) {
		this.reserva = reserva;
	}
}