package com.api.Entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotBlank;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;

@Entity
public class Servicos implements Serializable{
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long cod_Servico;
	
	@NotBlank(message="Campo obrigatório")
	private String nome_Servico;
	
	@NotBlank(message="Campo obrigatório")
	private String descricao_Servico;
	
	@NotBlank(message="Campo obrigatório")
	private String faixaEtaria;
	
	@ManyToOne
	@JoinColumn(name = "cod_Estabelecimento", nullable = true)
	@JsonBackReference
	private Estabelecimento cod_Estabelecimento;
	
	@ManyToMany(cascade=CascadeType.ALL)
	@JoinTable(name= "servico_reserva",
				joinColumns= {@JoinColumn(name="cod_servico")},
				inverseJoinColumns= {@JoinColumn(name="cod_reserva")})
	@JsonManagedReference
	private List<Reserva> reserva = new ArrayList<Reserva>();

	public Long getCod_Servico() {
		return cod_Servico;
	}

	public void setCod_Servico(Long cod_Servico) {
		this.cod_Servico = cod_Servico;
	}

	public String getNome_Servico() {
		return nome_Servico;
	}

	public void setNome_Servico(String nome_Servico) {
		this.nome_Servico = nome_Servico;
	}

	public String getDescricao_Servico() {
		return descricao_Servico;
	}

	public void setDescricao_Servico(String descricao_Servico) {
		this.descricao_Servico = descricao_Servico;
	}

	public String getFaixaEtaria() {
		return faixaEtaria;
	}

	public void setFaixaEtaria(String faixaEtaria) {
		this.faixaEtaria = faixaEtaria;
	}

	public Estabelecimento getCod_Estabelecimento() {
		return cod_Estabelecimento;
	}

	public void setCod_Estabelecimento(Estabelecimento cod_Estabelecimento) {
		this.cod_Estabelecimento = cod_Estabelecimento;
	}

	public List<Reserva> getReserva() {
		return reserva;
	}

	public void setReserva(List<Reserva> reserva) {
		this.reserva = reserva;
	}
}