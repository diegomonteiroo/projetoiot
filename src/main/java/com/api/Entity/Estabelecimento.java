package com.api.Entity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotBlank;

import com.fasterxml.jackson.annotation.JsonManagedReference;

@Entity
public class Estabelecimento implements Serializable {
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long cod_Estabelecimento;
	
	@NotBlank(message="*Campo obrigatório")
	@Column(unique=true)
	private String nome_Estabelecimento;
	
	@NotBlank(message="*Campo obrigatório")
	@Column(unique=true)
	private String cnpj_Estabelecimento;
	
	@NotBlank(message="*Campo obrigatório")
	private String end__Estabelecimento;
	
	@NotBlank(message="*Campo obrigatório")
	private String num_Estabelecimento;
	
	@NotBlank(message="*Campo obrigatório")
	private String tipo_Estabelecimento;
	
	@OneToMany(mappedBy="cod_Estabelecimento", targetEntity=Quartos.class, fetch=FetchType.LAZY, orphanRemoval = true, cascade=CascadeType.ALL)
	@JsonManagedReference
	private List<Quartos> quartos;
	
	@OneToMany(mappedBy="cod_Estabelecimento", targetEntity=Servicos.class, fetch=FetchType.LAZY, orphanRemoval = true, cascade=CascadeType.ALL)
	@JsonManagedReference
	private List<Servicos> servicos;

	public Long getCod_Estabelecimento() {
		return cod_Estabelecimento;
	}

	public void setCod_Estabelecimento(Long cod_Estabelecimento) {
		this.cod_Estabelecimento = cod_Estabelecimento;
	}

	public String getNome_Estabelecimento() {
		return nome_Estabelecimento;
	}

	public void setNome_Estabelecimento(String nome_Estabelecimento) {
		this.nome_Estabelecimento = nome_Estabelecimento;
	}

	public String getCnpj_Estabelecimento() {
		return cnpj_Estabelecimento;
	}

	public void setCnpj_Estabelecimento(String cnpj_Estabelecimento) {
		this.cnpj_Estabelecimento = cnpj_Estabelecimento;
	}

	public String getEnd__Estabelecimento() {
		return end__Estabelecimento;
	}

	public void setEnd__Estabelecimento(String end__Estabelecimento) {
		this.end__Estabelecimento = end__Estabelecimento;
	}

	public String getNum_Estabelecimento() {
		return num_Estabelecimento;
	}

	public void setNum_Estabelecimento(String num_Estabelecimento) {
		this.num_Estabelecimento = num_Estabelecimento;
	}

	public String getTipo_Estabelecimento() {
		return tipo_Estabelecimento;
	}

	public void setTipo_Estabelecimento(String tipo_Estabelecimento) {
		this.tipo_Estabelecimento = tipo_Estabelecimento;
	}

	public List<Servicos> getServicos() {
		return servicos;
	}

	public void setServicos(List<Servicos> servicos) {
		this.servicos = servicos;
	}
}