package com.api.Entity;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.validation.constraints.NotBlank;

import com.fasterxml.jackson.annotation.JsonBackReference;

@Entity
public class Quartos implements Serializable {
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long cod_Quarto;
	
	@NotBlank(message="*Campo obrigatório")
	private String nome_Quarto;
	
	@NotBlank(message="*Campo obrigatório")
	private String valorDiaria_Quarto;
	
	@NotBlank(message="*Campo obrigatório")
	private String data_Inicio_Reserva;
	
	@NotBlank(message="*Campo obrigatório")
	private String data_Fim_Reserva;
	
	@NotBlank(message="*Campo obrigatório")
	private String quant_hospedes_Quarto;
	
	@ManyToOne
	@JoinColumn(name="cod_Estabelecimento", nullable = false)
	@JsonBackReference
	private Estabelecimento cod_Estabelecimento;
	
	@OneToOne(mappedBy = "quarto")
	private Reserva reserva;

	public Long getCod_Quarto() {
		return cod_Quarto;
	}

	public void setCod_Quarto(Long cod_Quarto) {
		this.cod_Quarto = cod_Quarto;
	}

	public String getNome_Quarto() {
		return nome_Quarto;
	}

	public void setNome_Quarto(String nome_Quarto) {
		this.nome_Quarto = nome_Quarto;
	}

	public String getValorDiaria_Quarto() {
		return valorDiaria_Quarto;
	}

	public void setValorDiaria_Quarto(String valorDiaria_Quarto) {
		this.valorDiaria_Quarto = valorDiaria_Quarto;
	}

	public String getData_Inicio_Reserva() {
		return data_Inicio_Reserva;
	}

	public void setData_Inicio_Reserva(String data_Inicio_Reserva) {
		this.data_Inicio_Reserva = data_Inicio_Reserva;
	}

	public String getData_Fim_Reserva() {
		return data_Fim_Reserva;
	}

	public void setData_Fim_Reserva(String data_Fim_Reserva) {
		this.data_Fim_Reserva = data_Fim_Reserva;
	}

	public String getQuant_hospedes_Quarto() {
		return quant_hospedes_Quarto;
	}

	public void setQuant_hospedes_Quarto(String quant_hospedes_Quarto) {
		this.quant_hospedes_Quarto = quant_hospedes_Quarto;
	}

	public Estabelecimento getCod_Estabelecimento() {
		return cod_Estabelecimento;
	}

	public void setCod_Estabelecimento(Estabelecimento cod_Estabelecimento) {
		this.cod_Estabelecimento = cod_Estabelecimento;
	}

	public Reserva getQuarto() {
		return reserva;
	}

	public void setQuarto(Reserva reserva) {
		this.reserva = reserva;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
}