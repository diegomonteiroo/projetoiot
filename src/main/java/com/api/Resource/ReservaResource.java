package com.api.Resource;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.api.Entity.Reserva;
import com.api.Repository.ReservaRepository;

@RestController
public class ReservaResource {
	@Autowired
	ReservaRepository  rr;
	
	@RequestMapping(method = RequestMethod.GET, value = "/api/reservas", produces = "application/json")
	public List<Reserva> getAllServicos(){
		return rr.findAll();
	}
}