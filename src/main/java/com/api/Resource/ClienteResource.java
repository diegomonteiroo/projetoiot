package com.api.Resource;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.api.Entity.Cliente;
import com.api.Repository.ClienteRepository;

import javassist.tools.rmi.ObjectNotFoundException;

@RestController()
@RequestMapping(value="/api")
public class ClienteResource {
	@Autowired
	private ClienteRepository cr;
	
	//Lista todos os clientes
	@RequestMapping(value="/clientes", method=RequestMethod.GET, produces="application/json")
	public @ResponseBody List<Cliente> getAllClient(){
		return cr.findAll();
	}
	
	//Lista um cliente por id
	@RequestMapping(method = RequestMethod.GET, value = "/clientes/{id}", produces = "application/json")
	public Cliente getFindById(@PathVariable Long id) throws ObjectNotFoundException {
		Optional<Cliente> cli = cr.findById(id);
		return cli.orElseThrow(() -> new ObjectNotFoundException("Cliente não encontrado! ID:" + id 
				+ ", Tipo: " + Cliente.class.getName()));
	}
	
	//Criar um novo Cliente
	@RequestMapping(method = RequestMethod.POST, value = "/clientes", produces = "application/json")
	public ResponseEntity<Cliente> newCliente(@RequestBody Cliente cli){
		cr.save(cli);
		return new ResponseEntity<Cliente>(cli, HttpStatus.OK);
	}
	
	//Deletar um cliente
	@RequestMapping(method = RequestMethod.DELETE, value = "/clientes/{id}", produces = "application/json")
	public ResponseEntity<Cliente> deleteCliente(@PathVariable Long id) {
		cr.deleteById(id);
		return new ResponseEntity<Cliente>(HttpStatus.OK);
	}
	
	//Atualiza um cliente já existente
	@RequestMapping(method = RequestMethod.PUT, value = "/clientes/{id}", produces = "application/json")
	public ResponseEntity<Object> updateCLiente(@RequestBody Cliente cli, @PathVariable Long id){
		Optional<Cliente> clin = cr.findById(id);
		
		if(!clin.isPresent())
			return ResponseEntity.notFound().build();
		
		cli.setCod_Cliente(id);
		cr.save(cli);
		return ResponseEntity.noContent().build();
	}
}