package com.api.Resource;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.api.Entity.Servicos;
import com.api.Repository.ServicosRepository;

import javassist.tools.rmi.ObjectNotFoundException;

@RestController
public class ServicoResource {
	@Autowired
	ServicosRepository sr;
	
	//Listar todos os metodos
	@RequestMapping(method = RequestMethod.GET, value = "api/servicos", produces = "application/json")
	public @ResponseBody List<Servicos> getAllServicos(){
		return sr.findAll();
	}
	
	//Listar um serviço
	@RequestMapping(method = RequestMethod.GET, value = "/api/servicos/{id}", produces = "application/json")
	public Servicos findByIdQuartos(@PathVariable Long id) throws ObjectNotFoundException {
		Optional<Servicos> serv = sr.findById(id);
		return serv.orElseThrow(() -> new ObjectNotFoundException("Serviço não encontrado: " + id 
				+ ", Tipo: " + Servicos.class.getName()));
	}
	
	//Criar um novo serviço
	@RequestMapping(method = RequestMethod.POST, value = "/api/servicos", produces = "application/json")
	public ResponseEntity<Servicos> newServico(@RequestBody Servicos servicos){
		sr.save(servicos);
		return new ResponseEntity<Servicos>(HttpStatus.OK);
	}
	
	//Delete um serviço
	@RequestMapping(method = RequestMethod.DELETE, value = "/api/servicos/{id}", produces = "application/json")
	public ResponseEntity<Servicos> deleteServico(@PathVariable Long id){
		sr.deleteById(id);
		return new ResponseEntity<Servicos>(HttpStatus.OK);
	}
	
	//Atualizar um serviço
	@RequestMapping(method = RequestMethod.PUT, value = "/api/servicos/{id}", produces = "application/json")
	public ResponseEntity<Object> updateSesrvico(@RequestBody Servicos servico, @PathVariable Long id){
		Optional<Servicos> serv = sr.findById(id);
		
		if(!serv.isPresent())
			return ResponseEntity.notFound().build();
					
		servico.setCod_Servico(id);
		sr.save(servico);
		return ResponseEntity.noContent().build();
	}
}