package com.api.Resource;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.api.Entity.Comanda;
import com.api.Repository.ComandaRepository;

import javassist.tools.rmi.ObjectNotFoundException;

@RestController
@RequestMapping(value = "/api")
public class ComandaResource {
	@Autowired
	private ComandaRepository cr;
	
	//Buscar todos as comandas
	@RequestMapping(method = RequestMethod.GET, value = "/comandas", produces = "application/json")
	public @ResponseBody List<Comanda> getAllComandas(){
		return cr.findAll();
	}
	
	//Buscar comanda por id
	@RequestMapping(method = RequestMethod.GET, value = "/comandas/{id}", produces = "application/json")
	public Comanda getComandaFindById(@PathVariable Long id) throws ObjectNotFoundException {
		Optional<Comanda> com = cr.findById(id);
		return com.orElseThrow(() -> new ObjectNotFoundException("Comanda não encontrada! ID: " + id 
				+ ", Tipo: " + Comanda.class.getName()));
	}
	
	//Criar uma nova Comanda
	@RequestMapping(method = RequestMethod.POST, value = "/comandas", produces = "application/json")
	public ResponseEntity<Comanda> newComanda(@RequestBody Comanda com){
		cr.save(com);
		return new ResponseEntity<Comanda>(com, HttpStatus.OK);
	}
	
	//Deletar uma comanda
	@RequestMapping(method = RequestMethod.DELETE, value = "/comandas/{id}", produces = "application/json")
	public ResponseEntity<Comanda> deleteComanda(@PathVariable Long id){
		cr.deleteById(id);
		return new ResponseEntity<Comanda>(HttpStatus.OK);
	}
	
	//Atualizar uma comanda
	@RequestMapping(method = RequestMethod.PUT, value = "/comandas/{id}", produces = "application/json")
	public ResponseEntity<Object> updadeComanda(@RequestBody Comanda com, @PathVariable Long id){
		Optional<Comanda> coman = cr.findById(id);
		
		if(!coman.isPresent())
			return ResponseEntity.notFound().build();
		
		cr.save(com);
		return ResponseEntity.noContent().build();
	}
}